# Football t-shirts generation based on the general partner of a team

My assignment contains two distinct challenges: Feature Detection and Image Generation. In the realm of Feature Detection, the goal was to identify and extract different features embedded within images. The inherent challenge resided in selecting a model adept at detecting and identifying features within a given image.

The second task -- Image Generation, falls within the domain of Generative Machine Learning. Here, the goal was to generate new images that closely resemble the characteristics of a given dataset. The challenge involved developing a model capable of creating new images that exhibit a meaningful relationship with the underlying set of given images.

Based on my specifications the task has been divided into two parts: detection of a main partner on a t-shirt and generation of new ones based on t-shirts featuring the same partner.

Accesses to the datasets are here: 
Object detection dataset - https://drive.google.com/drive/folders/1t-hZyw-3nUk2UYP7TifdkTiiKec7WWby?usp=sharing \
Image generation dataset - https://drive.google.com/file/d/1mtDIKlVKSZPxzdwgkSnmyE4bvpYCgr1h/view?usp=sharing \
Saved model from object detection - https://drive.google.com/file/d/1L_rwZv8-SPS-f_KVZFH_6vTmDE5FUFn8/view?usp=sharing \
Label map - https://drive.google.com/file/d/1piZ8EMUnhhIF6uYztG7Tpb6Tyc-4PjPm/view?usp=sharing
